const _ = require('lodash');
const Printer = require('node-printer');
const options = {
    media: 'Custom.200x600mm',
    n: 3
};
const printer = new Printer('EPSON-TM-T82-S-A');

const billPrint = (order) => {
  const products = order.products.map(each => _.pick(each, ['name', 'quantity', 'basePrice', 'discount', 'tax', 'total']));
  // console.log(products);
  let nameArr = [], quantityArr = [], taxArr = [], totalArr = [];
  const nameS = 12, quantityS = 3, taxS = 5, totalS = 10;
  products.forEach(each => {
    let { name, quantity, basePrice, discount, tax, total } = each;
    tax = '₹' + (( basePrice * ( 1 - discount / 10000 ) * ( tax / 10000 )) / 100).toFixed(2); 
    name = name.toString();
    quantity = quantity.toString();
    total = '₹' + (total/100).toString();
    while ( (name + quantity + total).length > 0 ) {
      nameArr.push(name.slice(0, nameS));
      quantityArr.push(quantity.slice(0, quantityS));
      taxArr.push(tax.slice(0, taxS));
      totalArr.push(total.slice(0, totalS));
      name = name.slice(nameS);
      quantity = quantity.slice(quantityS);
      tax = tax.slice(taxS);
      total = total.slice(totalS);
    }
    nameArr.push('');
    quantityArr.push('');
    taxArr.push('');
    totalArr.push('');
  });
  string = `${'NAME'.padEnd(nameS)}  ${'QTY'.padEnd(quantityS)}  ${'TAX'.padEnd(taxS)}  ${'TOTAL'.padEnd(totalS)}\n\n`
  const tot = nameArr.length;
  for (let i = 0; i < tot; i++) {
    string = `${string}${nameArr.shift().padEnd(nameS)}  ${quantityArr.shift().padEnd(quantityS)}  ${taxArr.shift().padEnd(taxS)}  ${totalArr.shift().padEnd(totalS)}\n`;
  }
  if (order.discount) {
    const discount = '₹' + (order.discount / 100).toString()
    string = `${string}${'DISCOUNT'.padEnd(nameS + quantityS + taxS + 4)}  ${discount.padEnd(totalS)}\n`
  }
  if (order.tax) {
    const tax = '₹' + (order.tax / 100).toString()
    string = `${string}${'TAX'.padEnd(nameS + quantityS + taxS + 4)}  ${tax.padEnd(totalS)}\n`
  }
  if (order.total) {
    const total = '₹' + (order.total / 100).toString()
    string = `${string}${'TOTAL'.padEnd(nameS + quantityS + taxS + 4)}  ${total.padEnd(totalS)}\n`
  }
  // console.log(string);
  var jobFromText = printer.printText(string);
}
billPrint(
  {
    "products": [
      {
        "name": "Epsom Salt",
        "basePrice": 4200,
        "quantity": 1,
        "discount": 700,
        "tax": 700,
        "total": 4179
      },
      {
        "name": "Rantac",
        "basePrice": 6500,
        "quantity": 1,
        "discount": 400,
        "tax": 700,
        "total": 6677
      }
    ],
    "total": 10856,
    "tax": 709,
    "discount": 554
  },
)